# Salesforce Extractor

## Description

Salesforce extractor for Bizzflow. You need to setup your client-id and client-secret in Salesforce developer settings as seen in [this thread](https://developer.salesforce.com/forums/?id=906F0000000AfcgIAC).

Another thing is to have your security token setup via **Setup > Personal Setup > My Personal Information > Reset My Security Token**.

## Note

*The security token needs to be refreshed every 3 months (you will know it is the time because the extractor will stop working).*

## Sample config

```json
{
    "username": "connect@bizztreat.com",
    "password": "<secret-passworod>",
    "security-token": "<security-token>",
    "client-id": "application-client-id",
    "client-secret": "application-secret",
    "endpoints": [{
        "name": "opportunity_history",
        "endpoint": "OpportunityHistory"
    }, {
        "name": "agents",
        "endpoint": "User"
    }, {
        "name": "account",
        "endpoint": "Account"
    }, {
        "name": "activity_history",
        "endpoint": "Account.ActivityHistory[ActivityHistories]"
    }, {
        "name": "opportunities",
        "endpoint": "Opportunity"
    }, {
        "name": "leads",
        "endpoint": "Lead"
    }],
    "debug": true,
    "base": "https://xom-materials.my.salesforce.com"
}
```

### Endpoints explanation

Endpoint is an object containing name (result table name) and endpoint, which can be either of following two:

1. **Object name**
    - works well for all standard objects (such as Opportunity, Account, User, ...)
2. **Object path**
    - BaseEndpoint.Endpoint[Reference]
    - e.g. in order to extract ActivityHistory from Account (which is refered to as ActivityHistories in Salesforce data), you would use following expression as your endpoint:
      - Account.ActivityHistory[ActivityHistories]
