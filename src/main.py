#!/usr/bin/env python3

##
## Salesforce extractor
##

## Default parameters
SALESFORCE_API_VERSION = "v45.0"
OUTPUT_DIRECTORY = "/data/out/tables"

import requests
import json
import os
import csv
import re
import sys

if (len(sys.argv)==2) and (os.path.exists(sys.argv[1])):
    ## Bypass config
    config_path = sys.argv[1]
else:
    config_path = "/config/config.json"

## Load configuration
if not os.path.exists(config_path):
    raise Exception("Configuration not specified")

with open(config_path) as conf_file:
    conf = json.loads(conf_file.read())

## Create output directory
if not os.path.exists(OUTPUT_DIRECTORY):
    os.makedirs(OUTPUT_DIRECTORY)

## Default headers for API calls
headers = {
    "Accept-Encoding": "gzip,deflate" ## Save some bandwidth
}


def debug(o):
    ## Print debug information (if configured)
    if conf["debug"]:
        print(o)

## Create session for cookies storage
s = requests.session()

## Login POST data
data = {
    "grant_type": "password",
    "client_id": conf["client-id"],
    "client_secret": conf["client-secret"],
    "username": conf["username"],
    "password": conf["password"]+conf["security-token"]
}

debug("Loging into Salesforce...")

resp = s.post("https://login.salesforce.com/services/oauth2/token",params=data,headers=headers)

auth = resp.json()

debug("Authorized OK")

## Take access token from response
headers["Authorization"] = "Bearer {0}".format(auth["access_token"])

#resp = s.get("https://xom-materials.my.salesforce.com/services/data/{0}/query/".format(SALESFORCE_API_VERSION),params={"q": "SELECT Id, (SELECT Subject, ActivityType FROM ActivityHistory) FROM Account"},headers=headers)
#resp = s.get("https://xom-materials.my.salesforce.com/services/data/{0}/sobjects/{1}/describe/".format(SALESFORCE_API_VERSION,"ActivityHistory"),headers=headers)
#print(resp.text)
#exit()

## Get fieldset from specified sobject
def get_fieldset(sobject):
    global SALESFORCE_API_VERSION, headers
    resp = s.get("{0}/services/data/{1}/sobjects/{2}/describe/".format(conf["base"],SALESFORCE_API_VERSION,sobject),headers=headers)
    description = resp.json()
    if type(description)==list:
        if "errorCode" in description[0]:
            raise Exception(description[0]["message"])
    return [f["name"] for f in description["fields"]]

## Extract header from combined output
def get_combined_header(rows):
    header = []
    for row in rows:
        del row["attributes"]
        ## Skip if no joined result found
        joined_name = list(row.keys())[1]
        if row[joined_name]==None:
            continue
        for record in row[joined_name]["records"]:
            del record["attributes"]
            header = list(record.keys())
            ## One is enough
            break
        ## One is enough
        break
    return header

## Run extraction for each endpoint
for endpoint_setting in conf["endpoints"]:
    endpoint = endpoint_setting["endpoint"]
    debug("Extracting {0}".format(endpoint))
    if "." in endpoint:
        debug("Combined endpoint found")
        combined = True
        master, slave, reference = re.findall(r"(.*?)\.(.*?)\[(.*?)\]",endpoint)[0]
        # Get available endpoint fields for slave
        fieldlist = "Id, (SELECT {0} FROM {1})".format((",".join(get_fieldset(slave))),reference)
        output_name = endpoint_setting["name"]
    else:
        combined = False
        master = endpoint
        ## Get available endpoint fields for master
        fields = get_fieldset(master)
        fieldlist = ",".join(fields)
        output_name  = endpoint_setting["name"]

    ## Create SOQS query
    params = {
        "q": "SELECT {0} FROM {1}".format(fieldlist,master)
    }
    
    ## Get all objects
    resp = s.get("{0}/services/data/{1}/query/".format(conf["base"],SALESFORCE_API_VERSION),params=params,headers=headers)

    output = resp.json()

    ## Check for errors
    if type(output)==list:
        if "errorCode" in output[0]:
            raise Exception(output[0]["message"])

    ## Pagination
    records = []
    while True:
        ## Check if output is cool
        if not "records" in output:
            raise Exception("Failed extracting endpoint \'{0}\'".format(master))
        records.extend(output["records"])
        if "nextRecordsUrl" in output:
            debug("Next page found")
            debug("Next page url: {0}{1}".format(conf["base"],output["nextRecordsUrl"]))
            resp = s.get("{0}{1}".format(conf["base"],output["nextRecordsUrl"]),headers=headers)
            output = resp.json()
            ## Check for errors
            if type(output)==list:
                if "errorCode" in output[0]:
                    raise Exception(output[0]["message"])
            continue
        else:
            break
    print("Got {0} records".format(len(records)))
    ## Get header from fields
    header = list(records[0].keys())
    ## Remove metadata 'attributes' key from records
    header.remove("attributes")

    debug("Containing {0} columns".format(len(header)))

    ## Write output as csv
    output_filename = os.path.join(OUTPUT_DIRECTORY,output_name+".csv")
    debug("Saving output to \'{0}\'".format(output_filename))
    if "." in endpoint: combined_header = get_combined_header(records)
    with open(output_filename,"w") as outfile:
        writer = csv.writer(outfile, dialect=csv.unix_dialect)
        writer.writerow(header if not combined else combined_header)
        for record in records:
            if "attributes" in record:
                del record["attributes"]
            ## Ensure column order
            current_row = []
            if not combined:
                for column_name in header:
                    ## Clean & escape here if needed
                    val = record[column_name]
                    current_row.append(val)
                rows = [current_row]
            elif combined:
                rows = []
                if record[reference] == None:
                    print("No subrecords for record Id {0}, skipping".format(record["Id"]))
                    continue
                for subrecord in record[reference]["records"]:
                    current_row = []
                    for column_name in combined_header:
                        current_row.append(subrecord[column_name])
                    rows.append(current_row)
            writer.writerows(rows)
    debug("Written {0} rows".format(len(records)))
