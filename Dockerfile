FROM python:3.10-slim

RUN pip install requests

VOLUME /data/out/tables
VOLUME /config

ADD src/ /code

WORKDIR /code

# Edit this any way you like it
ENTRYPOINT ["python", "main.py"]

